package ictgradschool.industry.mock.test02.q2.portfolio;

import javax.swing.table.AbstractTableModel;

public class StockPortfolioAdapter extends AbstractTableModel implements StockPortfolioListener {

    private StockPortfolio myStockPortfolio;

    private String[] columnNames = {"Company name",
            "Number of share",
            "Buy price",
            "Current value"};

    public StockPortfolioAdapter(StockPortfolio myStockPortfolio) {
        this.myStockPortfolio = myStockPortfolio;
    }

    @Override
    public int getRowCount() {
        return myStockPortfolio.getNumberOfStocks();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

//    @Override
//    public Class<?> getColumnClass(int columnIndex) {
//
//        switch (columnIndex) {
//            case 0:
//                return String.class;
//            default:
//                return Integer.class;
//        }
//    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Stock myStock = myStockPortfolio.getStockAt(rowIndex);

        switch (columnIndex) {
            case 0:
                return myStock.getCompany();
            case 1:
                return myStock.getNumberOfShares();
            case 2:
                return myStock.getBoughtPrice();
            case 3:
                return myStock.getCurrentValue();
            default:
                return null;
        }

    }

    @Override
    public void update(Stock stock) {
        fireTableDataChanged();
    }
}
